from typing import TypedDict
import pandas as pd
import json
import os

class Data_Temp(TypedDict):
    tag: str
    patterns: list
    responses: list


datapath = os.path.join(os.path.dirname(__file__), "Materials.xlsx")
json_path = os.path.join(os.path.dirname(__file__), "Materials.json")
json_path2 = os.path.join(os.path.dirname(__file__), "intents.json")

excel_data_df = pd.read_excel(datapath)
excel_data_df.to_json(json_path, orient="records")
with open(json_path) as json_file:
    data = json.load(json_file)

data_template = {"intents": []}
for i, key in enumerate(data):

    data_line1 = data[i]["OBJ_ID_MAT"]
    data_line2 = data[i]["Object_Name"]
    data_line3 = data[i]["Dimensions"]
    data_line4 = data[i]["Unit_Of_Measure"]
    data_line5 = data[i]["International_Standard"]
    data_line6 = data[i]["Basic_Material2"]
    data_line7 = data[i]["Remarks"]
    data_line8 = data[i]["Mass"]
    data_line9 = data[i]["Density"]
    data_line10 = data[i]["eur_kg"]

    full_data = Data_Temp(tag=data_line1, patterns=[data_line1], responses=[
                  data_line1, data_line2, data_line3, data_line4, data_line5, data_line6, data_line7, data_line8, data_line9, data_line10])

    data_template["intents"].append(full_data)

with open(json_path2, "w") as newjson:
    json.dump(data_template, newjson)

