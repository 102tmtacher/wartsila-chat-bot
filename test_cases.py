import pytest
import converter
import chat
import train

def test_converter():
    assert "Materials.xlsx" in converter.datapath
    assert "Materials.json" in converter.json_path
    assert "intents.json" in converter.json_path2

def test_chat():
    assert "Bot" == chat.bot_name

def test_train():
    assert  train.device == "cuda" or "cpu"

